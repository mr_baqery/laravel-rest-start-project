<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;

class CollaborationMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $user;

    /**
     * Create a new message instance.
     *
     * @param $data
     * @param User $user
     */
    public function __construct($data, User $user)
    {
        $this->data = $data;
        $this->user = $user;
    }

    public function acceptUrl(): string
    {
        return URL::temporarySignedRoute(
            'collaboration.accept',
            Carbon::now()->addMinutes(Config::get('auth.verification.expire', 60)),
            [
                'id' => $this->user->getKey(),
                'hash' => sha1($this->user->getEmailForVerification()),
            ]
        );
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): CollaborationMail
    {
        return $this->from('info@info.com')
            ->subject("New Collaboration suggestion")
            ->view('collaboration', ["url" => $this->acceptUrl()])
            ->with('data', $this->data);
    }
}
