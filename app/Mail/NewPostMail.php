<?php

namespace App\Mail;

use App\Models\Post;
use App\Models\User;
use http\Url;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewPostMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $post;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param Post $post
     */
    public function __construct(User $user, Post $post)
    {
        $this->post = $post;
        $this->user = $user;
    }

    public function postUrl(): string
    {
        return "path to post url";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): NewPostMail
    {
        return $this->from('info@info.com')
            ->subject('New Post Created !')
            ->view('post_notif', ['user' => $this->user, 'postUrl' => $this->postUrl()]);
    }
}
