<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Utilities\ApiCode;
use Illuminate\Container\Container;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    public function login(LoginRequest $request): Response
    {

        $credentials = $request->only(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return $this->respondUnAuthenticated(ApiCode::INVALID_CREDENTIALS);
        }

        return $this->respondWithToken($token);
    }

    private function respondWithToken($token): Response
    {
        return $this->respond([
            'token' => $token,
            'access_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function logout(): Response
    {
        auth()->logout();

        return $this->respondWithMessage("User successfully logged out.");
    }

    public function refresh(): Response
    {
        return $this->respondWithToken(auth()->refresh());
    }

    public function me(): Response
    {
        return $this->respond(auth()->user());
    }
}
