<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Utilities\ApiCode;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth:api")->only(['resend']);
    }

    public function verify(Request $request, int $user_id)
    {
        if (!$request->hasValidSignature()) {
            $this->respondWithUnAuthorized(ApiCode::INVALID_EMAIL_VERIFICATION_URL);
        }

        $user = User::findOrFail($user_id);

        if (!$user->hasVerifiedEmail()) {
            $user->markEmailAsVerified();
        }

        return $this->respondWithMessage("User successfully verified.");
    }

    public function resend()
    {
        $user = auth()->user();

        if ($user->hasVerifiedEmail()) {
            return $this->respondBadRequest(ApiCode::EMAIL_ALREADY_VERIFIED);
        }

        $user->sendEmailVerifcationNotifaction();

        return $this->respondWithMessage("Verification email is now sent to your email.");

    }
}
