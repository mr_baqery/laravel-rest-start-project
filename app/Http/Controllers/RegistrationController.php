<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Models\User;
use App\Utilities\ApiCode;
use Illuminate\Auth\Events\Registered;
use Symfony\Component\HttpFoundation\Response;

class RegistrationController extends Controller
{

    public function register(RegisterRequest $request): Response
    {
        $input = $request->only(["name", "email", "password"]);

        $user = User::create($input);

        if ($user instanceof User) {
            $user->sendEmailVerificationNotification();
            return $this->respond($user, "User created successfully");
        }

        return $this->respondWithError(
            ApiCode::SOMETHING_WENT_WRONG,
            Response::HTTP_BAD_REQUEST
        );
    }
}
