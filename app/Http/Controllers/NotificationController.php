<?php

namespace App\Http\Controllers;

use App\Notifications\OffersNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Symfony\Component\HttpFoundation\Response;

class NotificationController extends Controller
{

    public function __construct()
    {
        $this->middleware("auth:api");
    }

    public function sendOfferNotification(): Response
    {
        $user = auth()->user();

        $offerData = [
            'name' => 'BOGO',
            'body' => 'You received an offer.',
            'thanks' => 'Thank you',
            'offerText' => 'Check out the offer',
            'offerUrl' => url('/'),
            'offer_id' => 007
        ];

        // $user->notify(new OffersNotification($offerData));

        Notification::send($user, new OffersNotification($offerData));

        return $this->respondWithMessage('The notification has been sent!');

    }
}
