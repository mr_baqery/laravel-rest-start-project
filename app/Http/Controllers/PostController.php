<?php

namespace App\Http\Controllers;

use App\Events\PostCreated;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function store()
    {
        $post = ['title' => "New post"];
        $post = Post::create($post);

        if ($post instanceof Post) {
            event(new PostCreated($post));
        }

    }
}
