<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendCollaborationEmailRequest;
use App\Mail\CollaborationMail;
use App\Models\User;
use App\Utilities\ApiCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SendCollaborationEmailController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api']);
    }

    public function send(SendCollaborationEmailRequest $request)
    {
        $current_user = auth()->user();

        $credentials = $request->validated();

        $credentials["from"] = $current_user->email;

        $user = User::byEmail($credentials['email']);

        $credentials["to"] = $user;

        Mail::to($user)
            ->send(new CollaborationMail($credentials, $current_user));

        return $this->respondWithMessage("Collaboration request has been sent.");
    }

    public function accept(Request $request)
    {
        $query_parameter = $request->all();

        $user = User::findOrFail($query_parameter['id']);

        if (!$user instanceof User) {
            $this->respondWithUnAuthorized(ApiCode::INVALID_CREDENTIALS);
        }

        if (!$request->hasValidSignature()) {
            $this->respondWithUnAuthorized(ApiCode::AUTHENTICATION_ERROR);
        }

        return $this->respondWithMessage('The request has been successfully accepted !');
    }
}
