<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewPasswordController;
use App\Http\Requests\NewPasswordRequest;
use App\Http\Requests\SendResetPasswordRequest;
use App\Utilities\ApiCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Symfony\Component\HttpFoundation\Response;

class ForgotPasswordController extends Controller
{
    public function forgot(SendResetPasswordRequest $request): Response
    {
        $credential = $request->validated();

        Password::sendResetLink($credential);

        return $this->respondWithMessage("Reset password link has been sent.");
    }

    public function reset(NewPasswordRequest $request)
    {
        $credential = $request->validated();

        $email_password_status = Password::reset($credential, function ($user, $password) {
            $user->password = $password;
            $user->save();
        });

        if ($email_password_status === Password::INVALID_TOKEN) {
            return $this->respondBadRequest(ApiCode::INVALID_RESET_PASSWORD_TOKEN);
        }

        return $this->respondWithMessage("Password successfully changed.");

    }
}
