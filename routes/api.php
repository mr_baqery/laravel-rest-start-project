<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\SendCollaborationEmailController;
use App\Http\Controllers\VerificationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['api'])->group(function ($router) {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/me', [AuthController::class, 'me']);
    Route::post('/email/send_collaboration', [SendCollaborationEmailController::class, 'send']);
    Route::get('/email/accept_collaboration', [SendCollaborationEmailController::class, 'accept'])->name('collaboration.accept');
});
Route::post('/post/create', [PostController::class, 'store']);
Route::post('/register', [RegistrationController::class, 'register']);
Route::get('/email/verify/{id}', [VerificationController::class, 'verify'])->name('verification.verify');
Route::get('/email/resend', [VerificationController::class, 'resend'])->name('verification.resend');
Route::post("/password/email", [ForgotPasswordController::class, 'forgot']);
Route::post("/password/reset", [ForgotPasswordController::class, 'reset'])->name('password.reset');
Route::get('/send-notification', [NotificationController::class, 'sendOfferNotification']);
