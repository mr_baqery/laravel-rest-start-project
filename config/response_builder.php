<?php

/**
 * Laravel API Response Builder - configuration file
 *
 * See docs/config.md for detailed documentation
 *
 * @author    Marcin Orlowski <mail (#) marcinOrlowski (.) com>
 * @copyright 2016-2021 Marcin Orlowski
 * @license   http://www.opensource.org/licenses/mit-license.php MIT
 * @link      https://github.com/MarcinOrlowski/laravel-api-response-builder
 */

return [
    /*
    |-------------------------------------------------------------------------------------------------------------------
    | Code range settings
    |-------------------------------------------------------------------------------------------------------------------
    */
    'min_code' => 100,
    'max_code' => 1024,

    /*
    |-------------------------------------------------------------------------------------------------------------------
    | Error code to message mapping
    |-------------------------------------------------------------------------------------------------------------------
    */
    'map' => [
        \App\Utilities\ApiCode::INVALID_CREDENTIALS => 'api.invalid_credentials',
        \App\Utilities\ApiCode::SOMETHING_WENT_WRONG => 'api.something_credentials',
        \App\Utilities\ApiCode::VALIDATION_ERROR => 'api.validation_error',
        \App\Utilities\ApiCode::AUTHENTICATION_ERROR => 'api.authentication_error',
        \App\Utilities\ApiCode::INVALID_EMAIL_VERIFICATION_URL => 'api.invalid_email_verification_url',
        \App\Utilities\ApiCode::EMAIL_ALREADY_VERIFIED => 'api.email_already_verified',
        \App\Utilities\ApiCode::INVALID_RESET_PASSWORD_TOKEN => "api.invalid_reset_password_token"
    ],

];
